# CS3060_FA2018_A02_ZIMCOSKY

## Code Summary

All tasks are coded, organized, labeled, and documented, in the file 'a2.pl'.

Under Task 1 is a rule that will reverse the elements of a list:

```prolog
% --------------------------------------------
%                    Task 1
% --------------------------------------------

reverseList([],[]).    % An empty list reverses to an empty list.

reverseList(L1, L2) :-  % If list 2 is the reverse of list 1, 
    reverseList(L1,[],L2).  % a temp list must be used to accumulate elements.

reverseList([H|T], Temp, L2) :-     % When given an occupied initial list,
    reverseList(T, [H|Temp], L2).    % that list's head becomes Temp's head.

reverseList([],L2,L2).  % If initial list empties, Temp is now our reversed list
```

Under Task 2 is a rule that will find the smallest element of a list:

```prolog
%--------------------------------------------
%                    Task 2
%--------------------------------------------

% smallest item of a 1 item list is that item. (base case)
smallest([Item], Item).

%  When given a full list, the two head elements are compared
smallest([H1,H2|T], Smallest) :- 
    H1 =< H2,   % If H1 is smaller,
    smallest([H1|T],Smallest).  % It will be the head in the next comparison

smallest([H1,H2|T], Smallest) :- 
    H1 > H2, % If H2 is smaller,
    smallest([H2|T],Smallest).  % It will be the head in the next comparison
```

Under Task 3 is a knowledge base of cities with non stop flights to eachother.
Below these facts, is a recursive predicate that tells us which cities we can travel between by chaining routes together.

```prolog
%--------------------------------------------
%                    Task 3
%--------------------------------------------

%% knowlege base
nonStopFlight(pittsburgh, cleveland).
nonStopFlight(philadelphia, pittsburgh).
nonStopFlight(columbus, philadelphia).
nonStopFlight(sanFrancisco, columbus).
nonStopFlight(detroit, sanFrancisco).
nonStopFlight(toledo, detroit).
nonStopFlight(houston, toledo).

% we can travel between cities that have nonstop flights to eachother
findFlight(X,Y) :- nonStopFlight(X,Y).

  
findFlight(X,Y) :-  % you can travel from X to Y 
    nonStopFlight(X,Z), % if there is a nonstop flight from X,
    findFlight(Z,Y).    % to a city that you can travel to Y.
```

##Running 

This code was created, compiled, and tested using GNU Prolog on a bash shell. Below are examples of how to run the program, along with each task, with sample output.

### Compiling Code

```bash
Ryans-MacBook-Pro:cs3060_fa2018_a02_zimcosky ryanzimcosky$ gprolog a1.pl
GNU Prolog 1.4.5 (64 bits)
Compiled Aug 20 2018, 15:27:00 with clang
By Daniel Diaz
Copyright (C) 1999-2018 Daniel Diaz
| ?- ['a2.pl'].
compiling /Users/ryanzimcosky/cs3060_fa2018_a02_zimcosky/a2.pl for byte code...
/Users/ryanzimcosky/cs3060_fa2018_a02_zimcosky/a2.pl compiled, 54 lines read - 3099 bytes written, 6 ms

(1 ms) yes
```

### Task 1


```bash
| ?- reverseList([],X).

X = [] ? a

X = []

yes
| ?- reverseList([],X).

X = [] ? ;

X = []

(1 ms) yes
| ?- reverseList([],X).

X = [] ? 

yes
| ?- reverseList([], []).

true ? 

yes
| ?- reverseList([1,2,3,4,5,6,7,8,9,0], X). 

X = [0,9,8,7,6,5,4,3,2,1]

yes
| ?- reverseList([1,2,3,4,5,6,7,8,9,0], [0,9,8,7,6,5,4,3,2,1]).

yes
| ?- reverseList([1,2,3,4,5,6,7,8,9,0], [a,b,c,d,e,f,g,h,i,j]).

no
| ?- reverseList([1,2,3,4,5,6,7,8,9,0], [1,2,3,4,5,6,7,8,9,0]).

no

```

### Task 2

```bash
| ?- smallest([54,45,123,22,69,711],X).                    

X = 22 ? 

yes
| ?- smallest([54,45,123,22,69,711],22).

true ? 

yes
| ?- smallest([987],X).                                    

X = 987 ? 

yes
| ?- smallest([],X).                                       

no

```

### Task 3

```bash
| ?- findFlight(houston, toledo). 

true ? 

yes
| ?- findFlight(houston, detroit).

true ? 

yes
| ?- findFlight(houston, cleveland).

true ? 

yes
| ?- findFlight(houston, Where).    

Where = toledo ? 

yes
| ?- 

findFlight(houston, Where).

Where = toledo ? a

Where = detroit

Where = sanFrancisco

Where = columbus

Where = philadelphia

Where = pittsburgh

Where = cleveland

no
```
