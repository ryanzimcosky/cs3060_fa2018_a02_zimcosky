% Ryan Zimcosky
% Assignment 2


% --------------------------------------------
%                    Task 1
% --------------------------------------------

reverseList([],[]).    % An empty list reverses to an empty list.

reverseList(L1, L2) :-  % If list 2 is the reverse of list 1, 
    reverseList(L1,[],L2).  % a temp list must be used to accumulate elements.

reverseList([H|T], Temp, L2) :-     % When given an occupied initial list,
    reverseList(T, [H|Temp], L2).    % that list's head becomes Temp's head.

reverseList([],L2,L2).  % If initial list empties, Temp is now our reversed list 

%--------------------------------------------
%                    Task 2
%--------------------------------------------

% smallest item of a 1 item list is that item. (base case)
smallest([Item], Item).

%  When given a full list, the two head elements are compared
smallest([H1,H2|T], Smallest) :- 
    H1 =< H2,   % If H1 is smaller,
    smallest([H1|T],Smallest).  % It will be the head in the next comparison

smallest([H1,H2|T], Smallest) :- 
    H1 > H2, % If H2 is smaller,
    smallest([H2|T],Smallest).  % It will be the head in the next comparison

%--------------------------------------------
%                    Task 3
%--------------------------------------------

%% knowlege base
nonStopFlight(pittsburgh, cleveland).
nonStopFlight(philadelphia, pittsburgh).
nonStopFlight(columbus, philadelphia).
nonStopFlight(sanFrancisco, columbus).
nonStopFlight(detroit, sanFrancisco).
nonStopFlight(toledo, detroit).
nonStopFlight(houston, toledo).

% we can travel between cities that have nonstop flights to eachother
findFlight(X,Y) :- nonStopFlight(X,Y).

  
findFlight(X,Y) :-  % you can travel from X to Y 
    nonStopFlight(X,Z), % if there is a nonstop flight from X,
    findFlight(Z,Y).    % to a city that you can travel to Y.
